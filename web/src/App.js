import Reacct, { useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  useLocation,
  useHistory,
} from "react-router-dom";

import Navbar from './components/Navbar';
import NavbarBarrel from './components/NavbarBarrel';
import Home from './components/pages/Home';
import Services from './components/pages/Services';
import Products from './components/pages/Products';
import SignUp from './components/pages/SignUp';
import SignIn from './components/SignIn';
import Register from './components/Register';
import UserManagement from './components/UserManagement';
import './App.css';

function App() {
  const history = useHistory();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  return (
    <>
      <Router>
        {/* <Navbar /> */}
        <NavbarBarrel />
        <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/services' component={Services} />
          <Route path='/products' component={Products} />
          <Route path='/sign-up' component={SignUp} />
          <Route path='/sign-in' component={SignIn} />
          <Route path='/register' component={Register} />
          <Route path='/user-management' component={UserManagement} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
