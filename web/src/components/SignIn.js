import React, { useState, useEffect, useLayoutEffect, useInput } from "react";
import {
  Box,
  makeStyles,
  Container,
  Grid,
  Typography,
  Link,
  Modal,
} from "@material-ui/core";
import { useHistory } from "react-router-dom";
import request from "request";
// import Avatar from '@material-ui/core/Avatar';
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
// import FormControlLabel from '@material-ui/core/FormControlLabel';
// import Checkbox from '@material-ui/core/Checkbox';
// import LockOutlinedIcon from '@material-ui/icons/LockOutlined';

import { config } from "../config";
import Register from "./Register";

export default function SignIn() {
  const classes = useStyles();
  const history = useHistory();
  const [isPhoneCorrect, setisPhoneCorrect] = useState(false);
  const [values, setValues] = useState({
    phone: "",
  });
  const [openRegister, setopenRegister] = useState(false);

  useEffect(() => {
    console.log(values);
  }, [values]);

  const handleClickRegister = () => {
    // setopenRegister(true);
    history.push("/register");
  };

  const handleChangePhone = (event) => {
    const phoneRegular = /^[0-9]{8}$/;
    const tel = event.target.value;

    if (phoneRegular.test(tel) && tel.length < 9) {
      setValues({
        ...values,
        [event.target.name]: event.target.value,
      });
      setisPhoneCorrect(phoneRegular.test(tel));
    } else {
      setisPhoneCorrect(false);
    }
  };

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value,
    });
  };

  const _signIn = () => {
    return new Promise((resolve, reject) => {
      request(
        {
          // uri: config.server.host,
          uri: config.server.host + '/account/login',
          method: "POST",
          body: { phone: values.phone },
          json: true,
        },
        (error, response, body) => {
          if (error) {
            reject(error.message);
          } else if (response.statusCode != 200) {
            reject("Either your phone is incorrect! " + "Please try again.");
            console.log(response.statusCode);
          } else {
            localStorage.setItem("account", JSON.stringify(body));
            resolve(body);
            alert("SignIn success!");
          }
        }
      );
    })
      .then((account) => {
        if (account) {
          history.push("/");
        }
      })
      .catch((error) => {
        alert(error);
      });
  };

  return (
    <>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          {/* <Avatar className={classes.avatar}> */}
          {/* <LockOutlinedIcon /> */}
          {/* </Avatar> */}
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form} noValidate>
            {/* <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
          /> */}
            {/* <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
          /> */}
            <Grid container>
              <Grid item xs>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="phone"
                  label="Phone Number"
                  name="phone"
                  type="tel"
                  pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
                  error={!isPhoneCorrect}
                  helperText={isPhoneCorrect ? "OK" : "Incorrect phone entry."}
                  autoComplete="phone"
                  autoFocus
                  onInput={(event) =>
                    (event.target.value = parseInt(event.target.value) ? 
                    parseInt(event.target.value).toString().slice(0, 8)
                    : "")
                  }
                  onChange={handleChangePhone}
                />
              </Grid>
              <Grid item>
                <Button
                  color="primary"
                  className={classes.submit}
                  onClick={() => _signIn()}
                >
                  Sign In
                </Button>
              </Grid>
            </Grid>
            {/* <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          /> */}
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={() => handleClickRegister()}
            >
              Register
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2">
                  {/* Forgot password? */}
                </Link>
              </Grid>
              <Grid item>
                <Link href="#" variant="body2">
                  {/* {"Don't have an account? Sign Up"} */}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        {/* <Modal 
        open={openRegister}
      >
        <Register 
          close={() => setopenRegister(false)}
        />
      </Modal> */}
      </Container>
    </>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    backgroundColor: "white",
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: 20,
    borderRadius: 20,
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));
