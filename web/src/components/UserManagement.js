import React, { useContext, useState, useEffect } from "react";
import ".././App.css";
import { makeStyles } from "@material-ui/core/styles";
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';


import request from "request";
import { config } from "../config";

const useStyles = makeStyles({
  root: {
    background: 'url("./img/Banner03-01.jpg") ',
    maxWidth : '100%',
    minHeight : '100%',
  },
  labelText: {
    margin: '10px',
    fontFamily: 'DFKai-sb, Arial',
    fontSize: '1.2em',
    color : 'white'
  },
  labelButton: {
    margin: '20px',
  },
  formControl: {
    margin: '10px',
    width: '80px',
  },
  table: {
    minWidth: 300,
  }
});

export default function App() {
  const classes = useStyles();
  const [gender, setGender] = useState('');
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [phone, setPhone] = useState('');
  const [birthMonth, setBirthMonth] = useState('');
  const [birthDate, setBirthDate] = useState('');
  const [birthYear, setBirthYear] = useState('');
  const [resAccountsInfo ,setResAccountsInfo] = useState([]);
  const DateStrToStamp = (str) => {
    return new Date(str).getTime();
  }
  const handleStartDateChange = (event) => {
    setStartDate(DateStrToStamp(event.target.value));
  };
  const handleEndDateChange = (event) => {
    setEndDate(DateStrToStamp(event.target.value));
  };
  const handlePhoneChange = (event) => {
    setPhone(event.target.value);
  };
  const handleGenderChange = (event) => {
    setGender(event.target.value);
  };
  const handleBirthdayChange = (event) => {
    let birth_date = new Date(event.target.value);
    setBirthMonth(birth_date.getMonth()+1);
    setBirthDate(birth_date.getDate());
    setBirthYear(birth_date.getFullYear());
  }
  const apiRequest = () => {
    new Promise((resolve, reject) => {
      let params = {
        start :　startDate ,
        end : endDate ,
        phone : phone ,
        gender : gender ,
        birthMonth : birthMonth,
        birthDate : birthDate,
        birthYear : birthYear
      }
      request(
        {
          uri: config.server.host + '/account/info',
          method: "POST",
          body: params,
          json: true
        },
        (error, response, body) => {
          if (error) {
            reject(error.message);
          }
          else {
            console.log(response.body);
            setResAccountsInfo(response.body)
          }
        }
      );
    })
  }
  return (
    <div className={ classes.root }>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="flex-start"
        xs="12"
      >
          <div class="col">
            <div>
              <label className={classes.labelText} for="start_month">Select start date:</label>
              <input type="month" id="start_month" name="start_month" onChange={handleStartDateChange} />
            </div>
            <div>
              <label className={classes.labelText} for="end_month">Select end date:</label>
              <input type="month" id="end_month" name="end_month" onChange={handleEndDateChange} />
            </div>
            <div>
            <TextField   
              InputProps={
                {
                  className: {
                    color : 'white'
                  }
                }  
              }
              id="standard-basic" label="Phone" onChange={handlePhoneChange} />
            </div>
            <div>
            <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">gender</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={gender}
              onChange={handleGenderChange}
            >
          <MenuItem value={""}>All do</MenuItem>
          <MenuItem value={"Male"}>Male</MenuItem>
          <MenuItem value={"FeMale"}>Female</MenuItem>
        </Select>
        </FormControl>
            </div>
            <div>
              <label className={classes.labelText} for="birthday">Birthday:</label>
              <input type="date" id="birthday" name="birthday" onChange={handleBirthdayChange}/>
            </div>
            <Button className={classes.labelButton} variant="contained" color="primary" onClick={apiRequest}>Request</Button>
            <Button className={classes.labelButton} variant="contained" color="secondary">Clear</Button>
          </div>
      </Grid>
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
      >
        <TableContainer component={Paper}>
          <Table className={classes.table} size="small" aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell align="left">Register Date</TableCell>
                <TableCell align="left">Name</TableCell>
                <TableCell align="left">Gender</TableCell>
                <TableCell align="left">Email</TableCell>
                <TableCell align="left">Birthday</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
                {
                  resAccountsInfo.map((account,index) => 
                      <TableRow key={index}>
                        <TableCell>{account.created}m</TableCell>
                        <TableCell>{account.firstName} {account.lastName} </TableCell>
                        <TableCell> {account.gender}</TableCell>
                        <TableCell> {account.email}</TableCell>
                        <TableCell> {account.email}</TableCell>
                      </TableRow>
                    )
                }
            </TableBody>
          </Table>
        </TableContainer>
    </Grid>
    </div>
  );
}
