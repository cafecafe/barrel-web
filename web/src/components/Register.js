import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import request from "request";
// import Avatar from '@material-ui/core/Avatar';
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
// import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from "@material-ui/core/Typography";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

import { config } from "../config";

export default function Register(props) {
  const classes = useStyles();
  const history = useHistory();
  const [firstName, setfirstName] = useState("");
  const [lastName, setlastName] = useState("");
  const [gender, setgender] = useState("");
  const [year, setyear] = useState(1900);
  const [month, setmonth] = useState("Jan");
  const [day, setday] = useState(1);
  const [birthday, setbirthday] = useState({
    year: 1900,
    month: "Jan",
    day: 1,
  });
  const [phone, setphone] = useState("");
  const [isPhoneCorrect, setisPhoneCorrect] = useState(true);
  const [hasRead, sethasRead] = useState(false);

  const handleChangePhone = (event) => {
    const phoneRegular = /^[0-9]{8}$/;
    const tel = event.target.value;

    if (phoneRegular.test(tel) && tel.length < 9) {
      setphone(tel);
      setisPhoneCorrect(phoneRegular.test(tel));
    } else {
      setisPhoneCorrect(false);
    }
  };

  const handleChangeFirstName = (event) => {
    setfirstName(event.target.value);
  };

  const handleChangeLastName = (event) => {
    setlastName(event.target.value);
  };

  const handleChangeGender = (event) => {
    setgender(event.target.value);
  };

  const handleChangeYear = (event) => {
    setyear(event.target.value);
  };

  const handleChangeMonth = (event) => {
    setmonth(event.target.value);
  };

  const handleChangeDay = (event) => {
    setday(event.target.value);
  };

  const handleChangeBirthday = ({ year, month, day }) => {
    if (year) {
      setbirthday({});
    }
  };

  const handleBack = () => {
    history.goBack();
  };

  const _register = () => {
    if (!isPhoneCorrect || (phone.length == 0)) {
      alert("請檢查輸入!")
      return;
    }
    return new Promise((resolve, reject) => {
      request(
        {
          uri: config.server.host + "/account/signup",
          method: "POST",
          body: {
            phone: phone,
            firstName: firstName,
            lastName: lastName,
            gender: gender,
            year: year,
            month: month,
            day: day,
          },
          json: true,
        },
        (error, response, body) => {
          if (error) {
            reject(error.message);
          } else if (response.statusCode != 200) {
            reject(
              "Register Fail! " + "Please try again." + response.statusCode
            );
          } else {
            localStorage.setItem("account", JSON.stringify(body));
            resolve(body);
            alert("Register success!");
          }
        }
      );
    })
      .then((account) => {
        if (account) {
          history.push("/");
        }
      })
      .catch((error) => {
        alert(error);
      });
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        {/* <Avatar className={classes.avatar}> */}
        {/* <LockOutlinedIcon /> */}
        {/* </Avatar> */}
        <Typography component="h1" variant="h5">
          Register
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="phone"
                label="Phone Number"
                name="phone"
                type="tel"
                pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}"
                error={!isPhoneCorrect}
                helperText={isPhoneCorrect ? false : "Incorrect phone entry."}
                autoComplete="phone"
                autoFocus
                onInput={(event) =>
                  (event.target.value = parseInt(event.target.value) ? 
                  parseInt(event.target.value).toString().slice(0, 8)
                  : "")
                }
                onChange={handleChangePhone}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="First Name"
                name="firstName"
                autoFocus
                onChange={handleChangeFirstName}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Last Name"
                name="lastName"
                autoComplete="lastName"
                onChange={handleChangeLastName}
              />
            </Grid>
            <Grid item xs={12}>
              <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel id="demo-simple-select-outlined-label">
                  Gender
                </InputLabel>
                <Select
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  value={gender}
                  onChange={handleChangeGender}
                  label="Gender"
                >
                  <MenuItem value={0}>Secret</MenuItem>
                  <MenuItem value={1}>Male</MenuItem>
                  <MenuItem value={2}>Female</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                variant="outlined"
                id="year"
                label="Year"
                name="year"
                autoComplete="year"
                onChange={handleChangeYear}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel id="demo-simple-select-outlined-label">
                  Month
                </InputLabel>
                <Select
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  value={month}
                  onChange={handleChangeMonth}
                  label="Month"
                >
                  <MenuItem value={"January"}>Jan</MenuItem>
                  <MenuItem value={"February"}>Feb</MenuItem>
                  <MenuItem value={"March"}>Mar</MenuItem>
                  <MenuItem value={"April"}>Apr</MenuItem>
                  <MenuItem value={"May"}>May</MenuItem>
                  <MenuItem value={"June"}>Jun</MenuItem>
                  <MenuItem value={"July"}>Jul</MenuItem>
                  <MenuItem value={"August"}>Aug</MenuItem>
                  <MenuItem value={"September"}>Sep</MenuItem>
                  <MenuItem value={"October"}>Oct</MenuItem>
                  <MenuItem value={"November"}>Nov</MenuItem>
                  <MenuItem value={"December"}>Dec</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                variant="outlined"
                id="day"
                label="Day"
                name="day"
                autoComplete="day"
                onChange={handleChangeDay}
              />
            </Grid>
            <Grid item xs={12}>
              <FormControlLabel
                control={<Checkbox value="allowExtraEmails" color="primary" />}
                label="I want to receive inspiration, marketing promotions and updates via email."
                checked={hasRead}
                onChange={() => sethasRead(!hasRead)}
              />
            </Grid>
          </Grid>
          <Button
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={() => _register()}
          >
            Register
          </Button>
          <Button
            fullWidth
            variant="contained"
            color="inherit"
            className={classes.close}
            onClick={() => handleBack()}
          >
            Back
          </Button>
        </form>
      </div>
    </Container>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    backgroundColor: "white",
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: 20,
    borderRadius: 20,
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  close: {
    backgroundcolor: "red",
  },
  formControl: {
    //   margin: theme.spacing(1),
    minWidth: 110,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
