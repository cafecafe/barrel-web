
const MonthStrMap = new Array(
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
);
class Month {
	static map = MonthStrMap;
	static getMonthFromTimeStamp(time){
		let date = new Date( time);
		date.setHours(date.getHours()+8);
		console.log(date);
		return date.getMonth() +1;
	}
}
module.exports = Month;