const mysql = require("mysql2");

const config = require("./config");

class Database {
  static getCurrentConnection() {
    if (!Database._currentDatabase) {
      console.log("Construct Data base.");
      Database._currentDatabase = new Database();
    }

    return Database._currentDatabase.getConnection();
  }

  constructor() {
    this.connection = mysql.createConnection({
      host: config.database.host,
      user: config.database.user,
      password: config.database.password,
      database: config.database.database,
      port: config.database.port,
    });

    this.connection.connect();

    this.connection.query("CREATE DATABASE IF NOT EXISTS db_barrel;", () => {
      this.connection.changeUser({ database: "db_barrel" }, (err) => {
        if (err) throw err;

        this.connection.query(`
            CREATE TABLE IF NOT EXISTS account (
              created   TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
              id        INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
              firstName VARCHAR(255) NOT NULL,
              lastName  VARCHAR(255) NOT NULL,
              phone     VARCHAR(255) NOT NULL,
              email     VARCHAR(255) NULL,
              gender    VARCHAR(255) NOT NULL,
              year      VARCHAR(255) NOT NULL,
              month     VARCHAR(255) NOT NULL,
              day       VARCHAR(255) NOT NULL
            )
          `);
        this.connection.query(`
            CREATE TABLE IF NOT EXISTS login_record (
              created   TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
              id        INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
              accountId INT          NOT NULL
            )

        `)
      });
    });
  }

  getConnection() {
    return this.connection;
  }
}

module.exports = Database;
