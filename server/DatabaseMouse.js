const mysql = require('mysql');

const config = require('./config');

class Database {
  static getCurrentConnection() {
    if (!Database._currentDatabase) {
      Database._currentDatabase = new Database();
    }

    return Database._currentDatabase.getConnection();
  }

  constructor() {
    this.connection = mysql.createConnection({
      host: config.database.host,
      user: config.database.user,
      password: config.database.password,
    });

    this.connection.connect();
  }

  getConnection() {
    return this.connection;
  }
}

module.exports = Database;
