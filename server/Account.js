// const jwt = require('jsonwebtoken');

const config = require('./config');
const Database = require('./Database');
const Month  = require('./Month');
const connection = Database.getCurrentConnection();
class Account {

  static setAccountLoginTime(accountId) {

    connection.query(`INSERT INTO login_record(accountId) VALUES (?)`,[accountId]);
  }

  static getAllAccount() {
    return new Promise(async (resolve,reject) => {

      connection.query(`SELECT * from account`
        , []
        , (error, results) => {
          resolve(results);
        });
    })   
  }
  static getLoginRecord() {
    return new Promise(async (resolve,reject) => {

      connection.query(`SELECT * from login_record`
        , []
        , (error, results) => {
          resolve(results);
        });
    })   
  }
  static getAccountsByRegisterTime(accountInfo, start, end ) {
    console.log(start , end);
    let start_stamp = new Date(start);
    let end_stamp = new Date(end);
    end_stamp.setMonth(end_stamp.getMonth()+1);
    return accountInfo.filter(account => {
      return account.created >= start_stamp && account.created < end_stamp;
    })

  }
  static getAccountsByPhone(accountsInfo, phone) {
    return accountsInfo.filter( account=> account.phone == phone);
  }
  static getAccountsByGender(accountsInfo, sex) {
    return accountsInfo.filter( account=> account.gender == sex)
  }
  static getAccountsByBirthday(accountsInfo, month, date, year) {
    return accountsInfo.filter( account=> {
      return account.month == Month.map[month-1] 
        && account.day == date
        && account.year == year;

    });
  }
  static getAccountsByRegisterTimeOnMonth(accountInfo, current) {
    let AccountsInfo = [];
    let request_month = Month.getMonthFromTimeStamp(parseInt(current));
    let i = accountInfo.length;
    accountInfo.forEach( async (element, index) => {
      let current_month = Month.getMonthFromTimeStamp(element.created);
      console.log("current_month ",current_month);
      console.log("element.created" ,element.created);
      if(current_month ==  request_month)
        AccountsInfo.push(element);
    });
    return AccountsInfo;
  }
  static login(phone) {
    return new Promise((resolve, reject) => {
      connection.query(
        `SELECT * FROM account WHERE phone = ?;`,
        [phone],
        (error, results, fields) => {
          if (results && results.length > 0) {
            Account.setAccountLoginTime(results[0].id);

            resolve(new Account(
              results[0].firstName,
              results[0].lastName,
              results[0].phone,
              results[0].email,
              // token
            ));
          }
          else {
            reject();
          }
        }
      );
    });
    // return new Promise((resolve, reject) => {
    //   connection.query(
    //     `SELECT * FROM account WHERE email = ? AND password = ?;`,
    //     [username, password],
    //     (error, results, fields) => {
    //       if (results && results.length > 0) {
    //         // const token = jwt.sign({username: username}, config.jwt.secret);

    //         resolve(new Account(
    //           results[0].firstName,
    //           results[0].lastName,
    //           results[0].phone,
    //           results[0].email,
    //           token
    //         ));
    //       }
    //       else {
    //         reject();
    //       }
    //     }
    //   );
    // });
  }

  static signUp(firstName, lastName, phone, gender, year, month, day) {
    const connection = Database.getCurrentConnection();

    return new Promise((resolve, reject) => {
      connection.query(
        'SELECT * FROM account WHERE phone = ?',
        [phone],
        (error, results, fields) => {
          if (results.length > 0) {
            console.log(results)
            reject('This account already exists.');
          }
          else {
            connection.query(
              `
                INSERT INTO account
                (firstName, lastName, phone, gender, year, month, day)
                VALUES (?, ?, ?, ?, ?, ?, ?);
              `,
              [firstName, lastName, phone, gender, year, month, day],
              (error, results, fields) => {
                if (error) {
                  reject(error);
                }
                else {
                  // const token = jwt.sign({username: email}, config.jwt.secret);

                  resolve(
                    new Account(firstName, lastName, phone, gender, year, month, day)
                  );
                }
              }
            );
          }
        }
      );
    });
  }

  constructor(firstName, lastName, phone, gender, year, month, day) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.phone = phone;
    this.gender = gender;
    this.year = year;
    this.month = month;
    this.day = day;
  }
}

module.exports = Account;
