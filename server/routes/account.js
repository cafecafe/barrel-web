const express = require('express');
const router = express.Router();

const Account = require('./../Account');

router.post('/login', (req, res, next) => {
    console.log("sdfsdfdf");
  Account.login(
    req.body.phone
  )
  .then((account) => {
    if (account) {
      res.json(account);
    }
    else {
      res.status(400).end();
    }
  })
  .catch((error) => {
    console.log('Login block: '+error);
    res.status(400).end();
  });
});
router.post('/loginInfo',async (req, res, next) => {
  let loginInfo = await Account.getLoginRecord();
  if (loginInfo) {
    res.json(loginInfo);
  }
  else {
    res.status(400).end();
  }

});
router.post('/info', async (req, res, next) => {
  let AccountsInfo= await Account.getAllAccount();
  let reqData = req.body;
  console.log(req);
  if(reqData.start && reqData.end) {
    AccountsInfo = Account.getAccountsByRegisterTime(
      AccountsInfo, reqData.start, reqData.end
    )
  }
  else if(reqData.start){
      console.log("ddd ",reqData.start);
    AccountsInfo = Account.getAccountsByRegisterTimeOnMonth(
      AccountsInfo, reqData.start
    )
  }
  console.log("dd");
  if(reqData.phone)
    AccountsInfo = Account.getAccountsByPhone(AccountsInfo, reqData.phone);
  if(reqData.gender)
    AccountsInfo = Account.getAccountsByGender(AccountsInfo, reqData.gender);
  if(reqData.birthMonth&& reqData.birthDate && reqData.birthYear)
    AccountsInfo = Account.getAccountsByBirthday(AccountsInfo, 
      reqData.birthMonth, 
      reqData.birthDate,
      reqData.birthYear);

  if (AccountsInfo) {
    res.json(AccountsInfo);
  }
  else {
    res.status(400).end();
  }

});

router.post('/signup', (req, res, next) => {
  Account.signUp(
    req.body.firstName,
    req.body.lastName,
    req.body.phone,
    req.body.gender,
    req.body.year,
    req.body.month,
    req.body.day
  )
  .then((account) => {
    if (account) {
      res.json(account);
    }
    else {
      res.status(400).end();
    }
  })
  .catch((error) => {
    console.log(error);
    res.status(400).end();
  });
});

module.exports = router;
