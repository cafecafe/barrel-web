
// Express and Socket.IO
const express = require('express');
const app = express();
const http = require('http').createServer(app);
const path = require('path');

// package to enable cross-origin resource sharing
const cors = require('cors');

// package to parse HTTP request body
const bodyParser = require('body-parser');

// using JWT to authorize logged-in accounts
// const jwt = require('express-jwt');

// initialize the database connection
const Database = require('./Database');
const connection = Database.getCurrentConnection();

// import the configurations
const config = require('./config');

// routes to handle HTTP requests
const accountRouter = require('./routes/account');

// handling HTTP requests
app.use(cors());
app.use(bodyParser.json());
// app.use(jwt({secret: config.jwt.secret})
//   .unless({path: new RegExp('/game/log/[0-9]+')}));
// app.use(express.static(path.join(__dirname, 'build')));
app.use('/account', accountRouter);
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

http.listen(3001, () => {
  console.log('Server start on port:3001');
});