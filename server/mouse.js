const express = require('express');
const app = express();
const http = require('http').createServer(app);
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');


const mysql = require('mysql');
const db = mysql.createPool({
    host: '220.135.67.240',
    user: 'admin',
    password: 'dls69687477',
    database : 'barrel_db',
});

// db.connect();
app.get('/db', (req, res) => {
    const sqlInsert = "INSERT INTO admin_user (adm_act, adm_pwd, adm_name) VALUES (?, ?, ?)";
    db.query(sqlInsert, ['mouse', 'mice'], (err, result) => {
        res.send('Mouse');
        if (err) {
            console.log(err);
        }
    });
});

// import the configurations
const config = require('./config');

// routes to handle HTTP requests
const accountRouter = require('./routes/account');

// handling HTTP requests
app.use(cors());
app.use(bodyParser.json());
// app.use(jwt({secret: config.jwt.secret})
//   .unless({path: new RegExp('/game/log/[0-9]+')}));
// app.use(express.static(path.join(__dirname, 'build')));
app.use('/account', accountRouter);

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

http.listen(3001, () => {
  console.log('Server start on port:3001');
});